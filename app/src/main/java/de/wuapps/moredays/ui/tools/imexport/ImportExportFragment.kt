package de.wuapps.moredays.ui.tools.imexport

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.database.sqlite.SQLiteDatabase
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import de.wuapps.moredays.MainActivity
import de.wuapps.moredays.R
import de.wuapps.moredays.database.MoreDaysDatabase
import de.wuapps.moredays.database.SQLiteToExcel
import de.wuapps.moredays.database.SQLiteToExcel.ExportCustomFormatter
import de.wuapps.moredays.database.converter.DateTypeConverter.Companion.convertStringToDate
import de.wuapps.moredays.databinding.FragmentImportExportBinding
import de.wuapps.moredays.utilities.DATABASE_NAME
import java.io.File
import java.io.FileInputStream
import java.io.InputStream
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date


class ImportExportFragment : Fragment(), ExportListener {

    private val TAG = "ImportExportFragment"
    private var _binding: FragmentImportExportBinding? = null
    private val binding get() = _binding!!
    private var hasPermissions = false

    companion object {
        private val handler = Handler(Looper.getMainLooper())
    }

    private val requestPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            hasPermissions = if (isGranted) {
                Log.i("Permission: ", "Granted")
                true
            } else {
                Log.i("Permission: ", "Denied")
                false
            }
        }

    private var requestedMimeType = "application/octet-stream"
    private val openRestoreFileChooser =
        registerForActivityResult(ActivityResultContracts.OpenDocument()) { result ->
            if (result != null) {
                val inputStream = requireContext().contentResolver.openInputStream(result)!!
                binding.progressBarCyclic.visibility = View.VISIBLE
                Thread {
                    try {
                        doRestore(inputStream)
                        handler.post { this.onCompleted() }
                    } catch (e: Exception) {
                        inputStream.close()
                        handler.post { this.onError(e) }
                    }
                }.start()
            }
        }
    private val openFileCreator = registerForActivityResult(
        ActivityResultContracts.CreateDocument(requestedMimeType)
    ) { result ->
        if (result != null) {
            Log.d(TAG, requireContext().getDatabasePath(DATABASE_NAME).toString())
            val out = requireContext().contentResolver.openOutputStream(result)!!
            binding.progressBarCyclic.visibility = View.VISIBLE
            Thread {
                try {
                    doWithCreatedFile(out)
                    handler.post { this.onCompleted() }
                } catch (e: Exception) {
                    out.close()
                    handler.post { this.onError(e) }
                }
            }.start()

        }
    }
    private var doWithCreatedFile = { _: OutputStream -> }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentImportExportBinding.inflate(inflater, container, false)
        val view = binding.root
        binding.btnExportExcel.setOnClickListener {
            exportToExcel(binding.cbAllTables.isChecked)
        }
        binding.btnExportMd.setOnClickListener {
            exportToMd(binding.cbDesc.isChecked)
        }
        binding.btnBackup.setOnClickListener {
            backup()
        }
        binding.btnRestore.setOnClickListener {
            restore()
        }
        checkPermissions()
        return view
    }

    private fun exportToMd(isDescOrder: Boolean) {
        if (!hasPermissions) {
            actionFailed(getString(R.string.error_permission_denied))
            return
        }
        requestedMimeType = "text/markdown"
        doWithCreatedFile = { out: OutputStream -> doExportToMd(out, isDescOrder) }
        openFileCreator.launch(getExportFileBaseName("export") + ".md")
    }

    private fun getMoodEmoji(mood: Int): String {
        return when (mood) {
            1 -> getString(R.string.mood_face_1)
            2 -> getString(R.string.mood_face_2)
            3 -> getString(R.string.mood_face_3)
            4 -> getString(R.string.mood_face_4)
            5 -> getString(R.string.mood_face_5)
            else -> ""
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun doExportToMd(out: OutputStream, isDescOrder: Boolean) {
        val db = SQLiteDatabase.openOrCreateDatabase(
            requireContext().getDatabasePath(DATABASE_NAME).absolutePath,
            null
        )
        val journalEntries = HashMap<String, Triple<String, String, String>>()
        val scaleEntries = ArrayList<Triple<String, String, Float>>()
        val activityEntries = ArrayList<ActivityEntry>()
        val hundredPercentValue = HashMap<String, Int>()
        //goal.name, goal.points, symbol, Activity.name, Activity.points, timestamp
        val formatter = SimpleDateFormat("yyMMdd")
        var c = db.rawQuery("select date, points from HundredPercentValue order by date asc", null)
        try {
            if (c.moveToFirst()) {
                do {
                    val date: String = c.getString(0)
                    val points: Int = c.getInt(1)
                    hundredPercentValue.put(formatter.format(convertStringToDate(date)), points)
                } while (c.moveToNext())
            }
            c.close()

            c = db.rawQuery("SELECT date,note,mood, affirmation FROM Journal", null)
            if (c.moveToFirst()) {
                do {
                    val date: String = c.getString(0)
                    val note: String = c.getString(1)
                    val mood: Int = c.getInt(2)
                    val title: String = c.getString(3)
                    journalEntries.put(
                        formatter.format(convertStringToDate(date)),
                        Triple(title, note, getMoodEmoji(mood))
                    )
                } while (c.moveToNext())
            }
            c.close()

            c = db.rawQuery(
                "SELECT name, value, timestamp FROM ScaleEntry INNER JOIN Scale ON ScaleEntry.scale_id = Scale.uid",
                null
            )
            if (c.moveToFirst()) {
                do {
                    val name: String = c.getString(0)
                    val value: Float = c.getFloat(1)
                    val time: Long = c.getLong(2)
                    scaleEntries.add(
                        Triple(
                            formatter.format(Date(time)),
                            name, value
                        )
                    )
                } while (c.moveToNext())
            }
            c.close()

            c = db.rawQuery(
                "select goal.name, goal.points, Activity.name, Activity.points, timestamp from Goal, Activity, ActivityEntry where goal.uid=Activity.goal_id AND Activity.uid=ActivityEntry.activity_id and goal.is_old = 0",
                null
            )
            if (c.moveToFirst()) {
                do {
                    val goalName: String = c.getString(0)
                    val goalPoints: Int = c.getInt(1)
                    val activityName: String = c.getString(2)
                    val activityPoints: Int = c.getInt(3)
                    val timestamp: Long = c.getLong(4)
                    activityEntries.add(
                        ActivityEntry(
                            formatter.format(
                                Date(timestamp)
                            ),
                            goalName, goalPoints, activityName, activityPoints
                        )
                    )
                } while (c.moveToNext())
            }
            c.close()
            db.close()
            Log.d(TAG, "after all queries")
            val history = StringBuffer()
            val allDates = mutableSetOf<String>()
            allDates.addAll(journalEntries.keys)
            activityEntries.forEach { allDates.add(it.date) }
            val sortedDays = if (isDescOrder) allDates.sortedDescending() else allDates.sorted()
            history.appendLine("# moreDays")
            val localizedFormatter = SimpleDateFormat(getString(R.string.date_ymd))
            for (day in sortedDays) {//default ascending
                val journalTitle = journalEntries[day]?.first ?: ""
                val journalNote = journalEntries[day]?.second ?: ""
                val journalMood = journalEntries[day]?.third ?: ""
                val scale = StringBuffer()
                scaleEntries.filter { it.first == day }
                    .forEach { scale.append("${it.second}: ${it.third}; ") }
                var sumPoints = 0
                val activity = StringBuffer()
                val goals = StringBuffer()
                val hundredPercentPoints =
                    hundredPercentValue[hundredPercentValue.keys.last { it <= day }]
                        ?: hundredPercentValue.values.first()
                activityEntries.filter { it.date == day }.forEach {
                    sumPoints += it.activityPoints
                    activity.append("${it.activityName} (${it.activityPoints}); ")
                }
                activityEntries.filter { it.date == day }.groupBy {it.goalName}.forEach{
                    goals.append("${it.key}: ${it.value.sumOf { item -> item.activityPoints }}/${it.value.maxOf { item->item.goalPoints }}; ")
                }
                val reached = "- $sumPoints/$hundredPercentPoints"
                val trophy = if (sumPoints >= hundredPercentPoints) "🏆" else ""
                val localizedDate = localizedFormatter.format(convertStringToDate(day))
                history.appendLine("## ${trophy}${localizedDate} $journalMood")
                if (sumPoints > 0) {
                    history.appendLine(reached)
                    history.appendLine("- $goals")
                    history.appendLine("- $activity")
                }
                if (scale.isNotEmpty()) {
                    history.append("- $scale")
                    history.appendLine()
                }
                if (journalTitle.isNotEmpty())
                    history.appendLine("- ${journalTitle}")
                if (journalNote.isNotEmpty()) {
                    history.appendLine("- $journalNote")
                }
                history.appendLine()
            }
            val bf = out.bufferedWriter(Charsets.UTF_8)
            bf.use { bfw -> bfw.write(history.toString()) }
        } catch (ex: Exception) {
            c.close()
            if (db.isOpen) db.close()
            throw Exception(ex.message)
        }
    }

    private fun backup() {
        if (!hasPermissions) {
            actionFailed(getString(R.string.error_permission_denied))
            return
        }
        val alertDialog: AlertDialog.Builder =
            AlertDialog.Builder(requireNotNull(this.activity))
        alertDialog.setTitle(getString(R.string.backup))
        alertDialog.setMessage("${getString(R.string.explain_backup)} ${getString(R.string.question_continue)}")
        alertDialog.setPositiveButton(
            getString(R.string.yes)
        ) { _, _ ->
            requestedMimeType = "application/octet-stream"
            doWithCreatedFile = { out: OutputStream -> doBackup(out) }
            openFileCreator.launch(getExportFileBaseName("backup") + ".db")
        }
        alertDialog.setNegativeButton(
            getString(R.string.no)
        ) { _, _ -> }
        val alert: AlertDialog = alertDialog.create()
        alert.setCanceledOnTouchOutside(false)
        alert.show()

    }

    //Thanks to https://medium.com/dwarsoft/how-to-provide-backup-restore-feature-for-room-database-in-android-d87f111d9c64
    private fun doBackup(backupOutputStream: OutputStream) {
        //close the db to make sure all journaling is run and in the single db file see https://www.sqlite.org/wal.html
        MoreDaysDatabase.getInstance(requireContext().applicationContext).close()
        val dbFile = requireContext().applicationContext.getDatabasePath(DATABASE_NAME)
        val bufferSize = 8 * 1024
        val buffer = ByteArray(bufferSize)
        var bytesRead: Int
        val dbInputStream: InputStream = FileInputStream(dbFile)
        while (dbInputStream.read(buffer, 0, bufferSize).also { bytesRead = it } > 0) {
            backupOutputStream.write(buffer, 0, bytesRead)
        }
        backupOutputStream.flush()
        dbInputStream.close()
        backupOutputStream.close()
        //exit Application as the view models need to refresh and this would be a hassle, it is not
        //easy to reopen the database and refresh the models automatically
        restartApp()
    }


    private fun restore() {
        if (!hasPermissions) {
            actionFailed(getString(R.string.error_permission_denied))
            return
        }
        val alertDialog: AlertDialog.Builder =
            AlertDialog.Builder(requireNotNull(this.activity))
        alertDialog.setTitle(getString(R.string.restore))
        alertDialog.setMessage("${getString(R.string.explain_restore)} ${getString(R.string.question_continue)}")
        alertDialog.setPositiveButton(
            getString(R.string.yes)
        ) { _, _ ->
            openRestoreFileChooser.launch(arrayOf("application/octet-stream"))
        }
        alertDialog.setNegativeButton(
            getString(R.string.no)
        ) { _, _ -> }
        val alert: AlertDialog = alertDialog.create()
        alert.setCanceledOnTouchOutside(false)
        alert.show()
    }

    private fun doRestore(source: InputStream) {
        Log.i(TAG, "in do restore")
        source.use { input ->
            val dbUri = requireContext().getDatabasePath(DATABASE_NAME).toURI()
            MoreDaysDatabase.getInstance(requireContext().applicationContext).close()
            File(dbUri).outputStream().use { output ->
                input.copyTo(output)
            }

        }
        restartApp()
    }

    @SuppressLint("SimpleDateFormat")
    private fun getExportFileBaseName(suffix: String): String {
        return "MoreDays_${suffix}_${SimpleDateFormat("yyyy-MM-dd-HH_mm_ss").format(Calendar.getInstance().time)}"
    }

    private fun exportToExcel(allTables: Boolean) {
        if (!hasPermissions) {
            actionFailed(getString(R.string.error_permission_denied))
            return
        }
        requestedMimeType = "application/octet-stream"
        doWithCreatedFile = { out: OutputStream -> doExportToExcel(out, allTables) }
        openFileCreator.launch(getExportFileBaseName("export") + ".xls")
    }

    @SuppressLint("SimpleDateFormat")
    private fun doExportToExcel(out: OutputStream, allTables: Boolean) {
        var database: SQLiteDatabase? = null
        try {
            val formatter = SimpleDateFormat(getString(R.string.date_time))
            val calendar = Calendar.getInstance()
            database = SQLiteDatabase.openOrCreateDatabase(
                requireContext().getDatabasePath(DATABASE_NAME).absolutePath,
                null
            )
            val sqliteToExcel = SQLiteToExcel(
                database,
                out
            )
            sqliteToExcel.setCustomFormatter(object : ExportCustomFormatter {
                override fun process(columnName: String?, value: String?): String? {
                    var formattedValue = value
                    if (columnName.equals("timestamp") && !value.isNullOrBlank()) {
                        calendar.apply { timeInMillis = value.toLong() }
                        formattedValue = formatter.format(calendar.time)

                    }
                    if (columnName.equals("mood") && value == "-1") {
                        formattedValue = ""
                    }
                    return formattedValue
                }
            })


            sqliteToExcel.exportAllTables(allTables)
        } catch (ex: Exception) {
            if (database != null && database.isOpen) {
                database.close()
            }
            throw Exception(ex.message)
        }
    }

    private fun actionFailed(msg: String = "") {
        if (msg.isEmpty())
            Toast.makeText(requireContext(), getString(R.string.error_unknown), Toast.LENGTH_LONG)
                .show()
        else
            Toast.makeText(requireContext(), msg, Toast.LENGTH_LONG).show()

    }

    private fun checkPermissions() {
        val permission: String
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU)
            permission = Manifest.permission.READ_EXTERNAL_STORAGE
        else
            permission = Manifest.permission.READ_MEDIA_IMAGES
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                permission
            ) == PackageManager.PERMISSION_GRANTED
        )
            hasPermissions = true
        else
            requestPermissionLauncher.launch(permission)
    }

    override fun onCompleted() {
        binding.progressBarCyclic.visibility = View.GONE
        Snackbar.make(
            requireActivity().findViewById(R.id.container),
            getString(R.string.action_success),
            Snackbar.LENGTH_LONG
        ).show()

    }

    override fun onError(e: Exception?) {
        binding.progressBarCyclic.visibility = View.GONE
        if (e != null) {
            e.message?.let {
                Log.e(TAG, it)
                actionFailed(it)
            }
        }
    }

    private fun restartApp() {
//        restartIntent!!.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//        context.startActivity(restartIntent)
        val intent = Intent(requireActivity(), MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        requireActivity().finish()
        Runtime.getRuntime().exit(0)
    }
}

interface ExportListener {
    fun onCompleted()
    fun onError(e: Exception?)
}

data class ActivityEntry(
    val date: String,
    val goalName: String,
    val goalPoints: Int,
    val activityName: String,
    val activityPoints: Int
)