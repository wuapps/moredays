## Summary

_Please briefly describe what part of the code base needs to be refactored._

## Improvement

_Explain the benefits of refactoring this code.
See also https://about.gitlab.com/handbook/values/index.html#say-why-not-just-what_

## Risks

_Please list features that can break because of this refactoring and how you intend to solve that._

## Components

_List files or directories that will be changed by the refactoring._
