_Titel [feat/fix/docs/style/refactor/learn/orga/test]: summary, e.g. feat: Add mood to journal_

## Effects

_Briefly describe what this MR is about._

## Issues

_Link related issues below. Insert the issue link or reference after the word "Closes" if merging this should automatically close it._

## Review checklist

- [ ] Code inspection
- [ ] Änderungen angeschaut und Verbesserungen mit todo notiert, ggf. Pair-Programming mit Autor:in, Fokus auf
  - [ ] general architecture
  - [ ] readability
  - [ ] DRY
  - [ ] separation of concerns
  - [ ] consistency

Thanks for your MR, you're awesome! :+1:
